/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "app.h"

union pkt_stat {
	uint8_t byte;
	struct {
		uint8_t lqi:7;
		uint8_t crc_ok:1;
	} __packed;
};

union recv_packet {
	uint8_t buf[MAX_DATA_SIZE + 2]; // +2 bytes for status bytes
	struct {
		uint8_t len;
		uint8_t addr;
		uint8_t payload[PAYLOAD_SIZE];
		uint8_t rssi;
		union pkt_stat stat;
	} __packed;
};

void print_packet(union recv_packet *pkt)
{
	int i;
	uint8_t *pp;
	pp = pkt->payload;

	printk("Received Packet: len=%d -- addr=%02X ", pkt->len, pkt->addr);
	if (pkt->len > 1) {
		printk("-- payload=");
		for (i = 1; i < pkt->len ; i++) {
			printk("%02X ", *pp++);
		}
	}
	printk("-- rssi=%d -- ", rssi_to_dbm((uint8_t) *pp));
	pp++;
	printk("crc=%d -- lqi=%d\n", ((union pkt_stat *)pp)->crc_ok, ((union pkt_stat *)pp)->lqi);
}

void recv_thread(void *rf_device, void *p2, void *p3)
{
	int ret;
	union recv_packet pkt = {0};
	const struct device *rf = rf_device;

	printk("Starting receiver...\n");

	while(true){
		ret = rf_recv(rf, (union rf_packet *)&pkt);
		if(ret > 0) {
			print_packet(&pkt);
		}
		/*
		else if (ret <= 0) {
			printk("No packet received.\n");
			rf_print_status(rf);
		} else {
			printk("Discarding packet of size: %d.\n", pkt_buf.length);
		}
		*/

		k_msleep(SLEEP_INTERVAL_RECV);
	}
}
