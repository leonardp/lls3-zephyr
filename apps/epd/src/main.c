/*
 * Copyright (c) 2018 Jan Van Winkel <jan.van_winkel@dxplore.eu>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <device.h>
#include <drivers/display.h>
#include <lvgl.h>
#include <stdio.h>
#include <string.h>
#include <zephyr.h>

#define LOG_LEVEL CONFIG_LOG_DEFAULT_LEVEL
#include <logging/log.h>
LOG_MODULE_REGISTER(app);

void main(void)
{
	uint32_t count = 0U;
	char count_str[11] = {0};
	int rc;
	const struct device *display_dev;
	lv_obj_t *hello_world_label;
	lv_obj_t *count_label;

	display_dev = device_get_binding(CONFIG_LVGL_DISPLAY_DEV_NAME);

	if (display_dev == NULL) {
		LOG_ERR("device not found.  Aborting test.");
		return;
	}

	lv_obj_t *hello_world_button;
	hello_world_button = lv_btn_create(lv_scr_act(), NULL);
	lv_obj_align(hello_world_button, NULL, LV_ALIGN_CENTER, 0, 0);
	lv_btn_set_fit(hello_world_button, LV_FIT_TIGHT);
	hello_world_label = lv_label_create(hello_world_button, NULL);

	lv_label_set_text(hello_world_label, "Hello world!");
	lv_obj_align(hello_world_label, NULL, LV_ALIGN_CENTER, 0, 0);

	count_label = lv_label_create(lv_scr_act(), NULL);
	lv_obj_align(count_label, NULL, LV_ALIGN_IN_BOTTOM_MID, 0, -30);

	lv_task_handler();
	display_blanking_off(display_dev);

	while (1) {
		k_sleep(K_MSEC(2000));
		LOG_DBG("Running...");
		sprintf(count_str, "cnt: %d", count);
		//lv_obj_align(count_label, NULL, LV_ALIGN_IN_TOP_MID, 0, 10+count*10);
		lv_label_set_text(count_label, count_str);
		++count;
		lv_task_handler();
		if (!(count % 10)) {
			display_blanking_on(display_dev);
			display_blanking_off(display_dev);
		}
	}
}
