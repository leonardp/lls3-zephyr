#include "app.h"

#define ADC_RESOLUTION		12
#define ADC_GAIN		ADC_GAIN_1
#define ADC_REFERENCE		ADC_REF_INTERNAL
#define ADC_ACQUISITION_TIME	ADC_ACQ_TIME_DEFAULT

#define CHANNEL_ID		DT_IO_CHANNELS_INPUT_BY_IDX(DT_PATH(zephyr_user), 0)
#define SAMPLE_BUFFER_SIZE	1

const struct device *dev_adc = DEVICE_DT_GET(DT_PHANDLE(DT_PATH(zephyr_user), io_channels));

static int16_t sample_buffer[SAMPLE_BUFFER_SIZE];

struct adc_channel_cfg channel_cfg = {
	.gain = ADC_GAIN,
	.reference = ADC_REFERENCE,
	.acquisition_time = ADC_ACQUISITION_TIME,
	.channel_id =  CHANNEL_ID,
	.differential = 0
};

struct adc_sequence sequence = {
	.channels    = BIT(CHANNEL_ID),
	.buffer      = sample_buffer,
	.buffer_size = sizeof(sample_buffer),
	.resolution  = ADC_RESOLUTION,
};

static inline int adc_to_delay(int32_t adc)
{
	float tmp;
	uint8_t delay = 0;

	tmp = adc/60 - 1;

	if (tmp < 0) {
		delay = 0;
	} else if ( tmp > 60 ) {
		delay = 70;
	} else {
		delay = (uint8_t)tmp;
	}

	return delay;
}

void adc_thread(void *p1, void *p2, void *p3)
{
	int ret;

	while(true){
		ret = adc_read(dev_adc, &sequence);
		if (ret != 0) {
			printk("ADC reading failed with error %d.\n", ret);
		}

		int32_t raw_value = sample_buffer[0];

		// set play delay and set pause led if applicable
		global.play_delay = adc_to_delay(raw_value);
		//printk("ADC reading: %d\n", raw_value);
		//printk("Delay: %d\n", global.play_delay);
		if (global.play_delay > 60) {
			global.play_active = false;
			gpio_pin_set_dt(&global.indi, 1);
		} else {
			global.play_active = true;
			gpio_pin_set_dt(&global.indi, 0);
		}
		k_msleep(SLEEP_INTERVAL_MS);
	}
}

int adc_setup(void)
{
	int ret;

	if (!device_is_ready(dev_adc)) {
		printk("ADC device not found\n");
		return -ENODEV;
	}

	ret = adc_channel_setup(dev_adc, &channel_cfg);
	if (ret != 0) {
		return ret;
	}

	return ret;
}
