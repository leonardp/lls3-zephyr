#ifndef APP_CC1101_H_
#define APP_CC1101_H_

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>
#include <drivers/rf.h>
#include <drivers/rf/cc1101.h>
#include <usb/usb_device.h>
#include <drivers/uart.h>
#include <drivers/adc.h>
#include <stdio.h>

#define THREAD_STACK_SIZE	1024
#define SLEEP_INTERVAL_MS	100

int gpio_setup(void);
int adc_setup(void);
int play_setup(void);

void adc_thread(void *p1, void *p2, void *p3);
void play_thread(void *p1, void *p2, void *p3);

struct glob {
	struct gpio_dt_spec indi;
	uint8_t play_delay;
	bool play_active;

	// recv mutex and flag
	struct k_mutex recv_mutex;
	bool recv_flag;
};

extern struct glob global;

static inline int rssi_to_dbm(uint8_t rssi)
{
	int rssi_dbm;

	if (rssi >= 128){
		rssi_dbm = ((float)rssi - 256U) / 2;
	} else {
		rssi_dbm = (float)rssi / 2;
	}
	rssi_dbm = rssi_dbm - 74U;
	return rssi_dbm;
}

#endif // APP_CC1101_H_
