## lischra sender

### Connections
| lischra | connector | pcb | function (BEN-5M) | function (RLK-29) |
| ------- | --------- | --- | -----------------	| ----------------- |
| BN      | 1         | B/W | 24 V		| 24 V |
| BU      | 4         | BK  | 0 V		| 0 V |
| BK      | 2         | WH  | N.O. 		| N.C. |
| WH      | 5         | BK  | COM		| COM |
| GY      | 3         | BN  | N.C.		| N.O. |
