#include "app.h"

static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET(DT_NODELABEL(button0), gpios);
static const struct gpio_dt_spec relay = GPIO_DT_SPEC_GET(DT_NODELABEL(relay), gpios);
static struct gpio_callback button_cb_data;
static struct gpio_callback relay_cb_data;

static struct gpio_dt_spec led = GPIO_DT_SPEC_GET(DT_NODELABEL(led0), gpios);

void button_pressed(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
	if(global.triggered == false){
		if(k_sem_take(&global.trigger_sem, K_NO_WAIT) == 0){
			printk("Button pressed at %u -- toggling mode\n", k_cycle_get_32());
			gpio_pin_toggle_dt(&led);
			global.triggered = true;
			k_sem_give(&global.trigger_sem);
		}
	}
}

void relay_triggered(const struct device *dev, struct gpio_callback *cb,
		    uint32_t pins)
{
	if(global.triggered == false){
		if(k_sem_take(&global.trigger_sem, K_NO_WAIT) == 0){
			//printk("Relay triggered sending signal\n");
			global.triggered = true;
			k_sem_give(&global.trigger_sem);
		}
	}
}

int gpio_setup(void)
{
	int ret = 0;

	if (!device_is_ready(relay.port)) {
		printk("Error: button device %s is not ready\n",
		       relay.port->name);
		return -ENODEV;
	}

	ret = gpio_pin_configure_dt(&relay, (GPIO_INPUT | GPIO_INT_DEBOUNCE));
	if (ret != 0) {
		printk("Error %d: failed to configure %s pin %d\n",
		       ret, relay.port->name, relay.pin);
		return ret;
	}

	ret = gpio_pin_interrupt_configure_dt(&relay,
					      GPIO_INT_EDGE_TO_ACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure interrupt on %s pin %d\n",
			ret, relay.port->name, relay.pin);
		return ret;
	}

	gpio_init_callback(&relay_cb_data, relay_triggered, BIT(relay.pin));
	gpio_add_callback(relay.port, &relay_cb_data);
	printk("Set up relay input at %s pin %d\n", relay.port->name, relay.pin);

	if (!device_is_ready(button.port)) {
		printk("Error: button device %s is not ready\n",
		       button.port->name);
		return ret;
	}

	ret = gpio_pin_configure_dt(&button, (GPIO_INPUT | GPIO_INT_DEBOUNCE));
	if (ret != 0) {
		printk("Error %d: failed to configure %s pin %d\n",
		       ret, button.port->name, button.pin);
		return ret;
	}

	ret = gpio_pin_interrupt_configure_dt(&button,
					      GPIO_INT_EDGE_TO_ACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure interrupt on %s pin %d\n",
			ret, button.port->name, button.pin);
		return ret;
	}

	gpio_init_callback(&button_cb_data, button_pressed, BIT(button.pin));
	gpio_add_callback(button.port, &button_cb_data);
	printk("Set up button at %s pin %d\n", button.port->name, button.pin);

	if (!device_is_ready(led.port)) {
		printk("Error %d: LED device %s is not ready; ignoring it\n",
		       ret, led.port->name);
		led.port = NULL;
	}

	ret = gpio_pin_configure_dt(&led, GPIO_OUTPUT_INACTIVE);
	if (ret != 0) {
		printk("Error %d: failed to configure LED device %s pin %d\n",
		       ret, led.port->name, led.pin);
		led.port = NULL;
	}

	printk("Set up LED at %s pin %d\n", led.port->name, led.pin);

	return ret;
}
