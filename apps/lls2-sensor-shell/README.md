## Zephyr Sensor Shell

This is mostly derived from the [Zephyr sample code](https://docs.zephyrproject.org/latest/samples/sensor/sensor_shell/README.html) and intended to
showcase it's usage with Sensirion's SHT4X and SGP40 sensors.

### Building and Flashing

This project includes an overlay and a config for the [Seeeduino XIAO](https://docs.zephyrproject.org/latest/boards/arm/seeeduino_xiao/doc/index.html) as well as the
[Blackpill](https://docs.zephyrproject.org/latest/boards/arm/blackpill_f411ce/doc/index.html).

```
git clone https://gitlab.com/lls-open-source/zephyr-lls2-shell.git
west build -b seeeduino_xiao zephyr-lls2-shell -p
cp build/zephyr/zephyr.uf2 /run/media/<username>/Arduino
```

### Application Usage

The device should show up as an USB ACM device:
```
dmesg | tail
[136377.011907] usb 5-3.2: new full-speed USB device number 118 using xhci_hcd
[136377.112173] usb 5-3.2: New USB device found, idVendor=2fe3, idProduct=0100, bcdDevice= 2.05
[136377.112175] usb 5-3.2: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[136377.112176] usb 5-3.2: Product: Zephyr USB console
[136377.112176] usb 5-3.2: Manufacturer: ZEPHYR
[136377.112177] usb 5-3.2: SerialNumber: 69AEDB2D50573651
[136377.157230] cdc_acm 5-3.2:1.0: ttyACM0: USB ACM device
```

You should now be able to connect to it with your favourite serial port emulator:

```
picocom -b 115200 /dev/ttyACM0
uart:~$
```

You can now verify that both devices are present on the I2C bus:

```
uart:~$ i2c scan I2C_0
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:             -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- 44 -- -- -- -- -- -- -- -- -- -- --
50: -- -- -- -- -- -- -- -- -- 59 -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
2 devices found on I2C_0
```

and marked READY

```
uart:~$ device list
devices:
- EIC (READY)
- pinmux@41004480 (READY)
- pinmux@41004400 (READY)
- SERCOM4 (READY)
- sys_clock (READY)
- CDC_ACM_0 (READY)
- I2C_0 (READY)
- sgp40 (READY)
  requires: I2C_0
- sht40 (READY)
  requires: I2C_0
```

### Getting Sensor Data

```
uart:~$ sensor get sgp40
channel idx=29 gas_resistance = 30799.000000

uart:~$ sensor get sht40
channel idx=13 ambient_temp =  21.939803
channel idx=16 humidity =  28.390014
```
