/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <usb/usb_device.h>

void main(void)
{
	const struct device *dev;

	dev = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);
	if (dev == NULL || usb_enable(NULL)) {
		return;
	}
}
