#include "app.h"
#include <drivers/pwm.h>
#include <drivers/counter.h>

/*
 * Devices
 */
#define TIMER_NODE	DT_LABEL(DT_NODELABEL(timer))
#define SPEAKER_NODE	DT_ALIAS(speaker)

const struct device *speaker;
const struct device *timer;
struct counter_alarm_cfg alarm_cfg;

/*
 * PWM definitions
 */
#define PWM_CTLR	DT_PWMS_CTLR(SPEAKER_NODE)
#define PWM_CHANNEL	DT_PWMS_CHANNEL(SPEAKER_NODE)
#define PWM_FLAGS	DT_PWMS_FLAGS(SPEAKER_NODE)

#define MIN_PERIOD_USEC	(USEC_PER_SEC / 64U)
#define MAX_PERIOD_USEC	USEC_PER_SEC

/*
 * Music!
 */
#define ALARM_CHANNEL	0
#define NOTE_LENGTH_US	125
#define NOTE_PERIOD	10000
#define NOTES_REPEAT	2

#include "notes2.h"

static void pwm_update_interrupt(const struct device *counter_dev,
				      uint8_t chan_id, uint32_t ticks,
				      void *user_data)
{
	global.note_idx += 1;
	if (global.note_idx > NOTE_COUNT ) {
		global.note_idx = 0;
		global.notes_repeat += 1;
	}
	if (global.notes_repeat > NOTES_REPEAT ) {
		global.notes_repeat = 0;
		pwm_pin_set_usec(speaker, PWM_CHANNEL, NOTE_PERIOD, 0, PWM_FLAGS);
		return;
	}
	//printk("play: %02X\n", notes[global.note_idx]);
	pwm_pin_set_usec(speaker, PWM_CHANNEL, NOTE_PERIOD, notes[global.note_idx], PWM_FLAGS);
	counter_set_channel_alarm(timer, ALARM_CHANNEL, &alarm_cfg);
}

int ring_the_bell(void)
{
	int ret;

	printk("DING GONG!\n");
	//counter_set_channel_alarm(timer, ALARM_CHANNEL, &alarm_cfg);
	pwm_pin_set_usec(speaker, PWM_CHANNEL, NOTE_PERIOD, 0xAF, PWM_FLAGS);

	return 0;
}

void play_thread(void *p1, void *p2, void *p3)
{
	while(true){
		ring_the_bell();
		k_msleep(SLEEP_INTERVAL_MS*10);
	}
}

int play_setup(void)
{
	int ret;
	ret = 0;
	global.notes_repeat = 0;
	global.note_idx = 0;

	timer = device_get_binding(TIMER_NODE);
	if (timer == NULL) {
		printk("Error: timer not found\n");
		return -ENODEV;
	}

	alarm_cfg.flags = 0;
	alarm_cfg.ticks = counter_us_to_ticks(timer, NOTE_LENGTH_US);
	alarm_cfg.callback = pwm_update_interrupt;
	alarm_cfg.user_data = &alarm_cfg;

	/*
	ret = counter_set_channel_alarm(timer, ALARM_CHANNEL, &alarm_cfg);
	printk("Alarm setup: %u sec (%u ticks)\n",
	       (uint32_t)(counter_ticks_to_us(timer, alarm_cfg.ticks) / USEC_PER_SEC),
	       alarm_cfg.ticks);

	if (-EINVAL == ret) {
		printk("Alarm settings invalid\n");
		return ret;
	} else if (-ENOTSUP == ret) {
		printk("Alarm setting request not supported\n");
		return ret;
	} else if (ret != 0) {
		printk("Alarm setup Error\n");
		return ret;
	}
	*/

	speaker = DEVICE_DT_GET(PWM_CTLR);
	if (!device_is_ready(speaker)) {
		printk("Error: Speaker device %s is not ready\n", speaker->name);
		return -ENODEV;
	}

	printk("PWM MAX/MIN period: %d/%d \n", MAX_PERIOD_USEC, MIN_PERIOD_USEC);

	ret = pwm_pin_set_usec(speaker, PWM_CHANNEL, MAX_PERIOD_USEC, 0, PWM_FLAGS);
	if (ret != 0) {
		printk("PWM setup Error\n");
		return ret;
	}

	counter_start(timer);

	return ret;
}
