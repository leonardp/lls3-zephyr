/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "app.h"

struct glob global;

K_THREAD_STACK_DEFINE(recv_stack_area, THREAD_STACK_SIZE);
struct k_thread recv_thread_data;

K_THREAD_STACK_DEFINE(send_stack_area, THREAD_STACK_SIZE);
struct k_thread send_thread_data;

void main(void)
{
	const struct device *usb;
	const struct device *rf;

	if (gpio_setup() != 0){
		printk("Failed setting up GPIO devices!\n");
		return;
	}

	usb = DEVICE_DT_GET_ONE(zephyr_cdc_acm_uart);
	if (usb_enable(NULL)) {
		printk("Failed enabling USB device!\n");
		return;
	}

	while (!device_is_ready(usb)) {
		printk("Waiting for USB device...\n");
		k_sleep(K_MSEC(100));
	}

	rf = device_get_binding("CC1101");
	if (rf == NULL) {
		printk("Failed getting RF device!\n");
		return;
	}

	k_fifo_init(&global.input_fifo);
	global.timeout = 0;

	//k_tid_t recv_tid = k_thread_create(&recv_thread_data, recv_stack_area,
	k_thread_create(&recv_thread_data, recv_stack_area,
					 K_THREAD_STACK_SIZEOF(recv_stack_area),
					 recv_thread,
					 (void *)rf, NULL, NULL,
					 K_PRIO_PREEMPT(10), 0, K_NO_WAIT);

	k_thread_create(&send_thread_data, send_stack_area,
					 K_THREAD_STACK_SIZEOF(send_stack_area),
					 send_thread,
					 (void *)rf, NULL, NULL,
					 K_PRIO_PREEMPT(10), 0, K_NO_WAIT);

	k_thread_join(&recv_thread_data, K_FOREVER);
	k_thread_join(&send_thread_data, K_FOREVER);

	printk("Exiting main()...\n");
	return;
}
