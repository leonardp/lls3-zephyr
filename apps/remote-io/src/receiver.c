/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "app.h"

union pkt_stat {
	uint8_t byte;
	struct {
		uint8_t lqi:7;
		uint8_t crc_ok:1;
	} __packed;
};

union recv_packet {
	uint8_t buf[PACKET_LEN + 3]; // +1 byte for length byte itself; +2 bytes for status bytes
	 	struct {
		uint8_t len;
		uint8_t addr;
		uint32_t cmd;
		uint8_t rssi;
		union pkt_stat stat;
	} __packed;
};

void print_packet(union recv_packet *pkt)
{
	printk("Received Packet: len=%d -- addr=%02X -- cmd=%08X -- rssi=%d -- crc=%d -- lqi=%d\n",
			pkt->len, pkt->addr, pkt->cmd, pkt->rssi, rssi_to_dbm(pkt->stat.crc_ok), pkt->stat.lqi);
}

void recv_thread(void *rf_device, void *p2, void *p3)
{
	int i;
	int ret;
	union recv_packet pkt = {0};
	const struct device *rf = rf_device;

	printk("Starting receiver...\n");

	while(true){
		ret = rf_recv(rf, (union rf_packet *)&pkt);
		if(ret == PACKET_LEN) {
			print_packet(&pkt);

			for (i = 0; i < global.num_outputs; i++) {
				if(pkt.cmd == global.outputs[i].cmd) {
					printk("Channel \"%s\" activated at %u\n", global.outputs[i].label, k_cycle_get_32());
					gpio_pin_toggle_dt(&global.outputs[i].gpio);
				}
			}
		}
		else if (ret <= 0) {
			//printk("No packet received.\n");
		} else {
			printk("Discarding packet of size: %d.\n", pkt.len);
			rf_print_status(rf);
		}

		k_msleep(SLEEP_INTERVAL_RECV);
	}
}
