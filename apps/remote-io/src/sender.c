/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "app.h"

union send_packet {
	uint8_t buf[PACKET_LEN+1]; // +1 byte for length byte itself
	struct {
		uint8_t len;
		uint8_t addr;
		uint32_t cmd;
	} __packed;
};

void send_thread(void *rf_device, void *p2, void *p3)
{
	int ret;
	const struct device *rf = rf_device;
	union send_packet pkt;
	struct fifo_data_t  *fdata;

	pkt.len = PACKET_LEN;
	pkt.addr = PACKET_ADR;
	pkt.cmd = 0;

	printk("Starting sender...\n");

	while(true){
		fdata = k_fifo_get(&global.input_fifo, K_FOREVER);
		pkt.cmd = fdata->cmd;
		ret = rf_send(rf, (union rf_packet *)&pkt);
		printk("Sending cmd \"%08X\": %s! (ret == %d)\n", pkt.cmd, (ret == 0) ? "success" : "failed", ret);
	}
}
