/*
 * Copyright (c) 2021 Leonard Pollak
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT maxim_max31343

#include <device.h>
#include <drivers/counter.h>
#include <drivers/gpio.h>
#include <drivers/i2c.h>
#include <kernel.h>
#include <logging/log.h>
#include <sys/timeutil.h>
#include <sys/util.h>
#include <time.h>

#include <drivers/rtc/maxim_max31343.h>

LOG_MODULE_REGISTER(MAX31343, CONFIG_COUNTER_LOG_LEVEL);

/* Alarm channels */
#define ALARM1_ID			0
#define ALARM2_ID			1

/* Size of block when writing a whole struct */
#define RTC_TIME_REGISTERS_SIZE		sizeof(struct max31343_time_registers)
#define RTC_ALARM1_REGISTERS_SIZE	sizeof(struct max31343_alarm1_registers)
#define RTC_ALARM2_REGISTERS_SIZE	sizeof(struct max31343_alarm2_registers)

/* Largest block size */
#define MAX_WRITE_SIZE                  (RTC_TIME_REGISTERS_SIZE)

/* tm struct uses years since 1900 but unix time uses years since
 * 1970. MAX31343 default year is '0' so the offset is 70
 */
#define UNIX_YEAR_OFFSET		70

/* Macro used to decode BCD to UNIX time to avoid potential copy and paste
 * errors.
 */
#define RTC_BCD_DECODE(reg_prefix) (reg_prefix##_one + reg_prefix##_ten * 10)

struct max31343_config {
	struct counter_config_info generic;
	struct i2c_dt_spec bus;
	const struct gpio_dt_spec int_gpios;
};

struct max31343_data {
	const struct device *max31343;
	struct k_sem lock;
	struct max31343_status status_flags;
	struct max31343_config_registers cfg_registers;
	struct max31343_time_registers tm_registers;
	struct max31343_alarm1_registers alm1_registers;
	struct max31343_alarm2_registers alm2_registers;

	struct k_work alarm_work;
	struct gpio_callback int_callback;

	counter_alarm_callback_t counter_handler[2];
	uint32_t counter_ticks[2];
	void *alarm_user_data[2];
};

/** @brief Convert bcd time in device registers to UNIX time
 *
 * @param dev the MAX31343 device pointer.
 *
 * @retval returns unix time.
 */
static time_t decode_rtc(const struct device *dev)
{
	struct max31343_data *data = dev->data;
	time_t time_unix = 0;
	struct tm time = { 0 };

	time.tm_sec = RTC_BCD_DECODE(data->tm_registers.rtc_sec.sec);
	time.tm_min = RTC_BCD_DECODE(data->tm_registers.rtc_min.min);
	time.tm_hour = RTC_BCD_DECODE(data->tm_registers.rtc_hours.hr);
	/* MAX31343 default value for weekday is '1' */
	time.tm_wday = data->tm_registers.rtc_weekday.weekday - 1;
	time.tm_mday = RTC_BCD_DECODE(data->tm_registers.rtc_date.date);
	/* MAX31343 default value for month is '1' */
	time.tm_mon = RTC_BCD_DECODE(data->tm_registers.rtc_month.month) - 1;
	/* tm struct uses years since 1900 but unix time uses years since 1970 */
	time.tm_year = RTC_BCD_DECODE(data->tm_registers.rtc_year.year)
		+ UNIX_YEAR_OFFSET;

	time_unix = timeutil_timegm(&time);

	LOG_DBG("Unix time is %d\n", (uint32_t)time_unix);

	return time_unix;
}

/** @brief Encode time struct tm into max31343 rtc registers
 *
 * @param dev the MAX31343 device pointer.
 * @param time_buffer tm struct containing time to be encoded into max31343
 * registers.
 *
 * @retval return 0 on success, or a negative error code from invalid
 * parameter.
 */
static int encode_rtc(const struct device *dev, struct tm *time_buffer)
{
	struct max31343_data *data = dev->data;
	uint8_t year_since_epoch;

	if (time_buffer->tm_year < UNIX_YEAR_OFFSET) {
		return -EINVAL;
	}
	year_since_epoch = time_buffer->tm_year - UNIX_YEAR_OFFSET;

	data->tm_registers.rtc_sec.sec_one = time_buffer->tm_sec % 10;
	data->tm_registers.rtc_sec.sec_ten = time_buffer->tm_sec / 10;
	data->tm_registers.rtc_min.min_one = time_buffer->tm_min % 10;
	data->tm_registers.rtc_min.min_ten = time_buffer->tm_min / 10;
	data->tm_registers.rtc_hours.hr_one = time_buffer->tm_hour % 10;
	data->tm_registers.rtc_hours.hr_ten = time_buffer->tm_hour / 10;
	/* MAX31343 default value for weekday is '1' */
	data->tm_registers.rtc_weekday.weekday = time_buffer->tm_wday + 1;
	data->tm_registers.rtc_date.date_one = time_buffer->tm_mday % 10;
	data->tm_registers.rtc_date.date_ten = time_buffer->tm_mday / 10;
	/* MAX31343 default value for month is '1' */
	data->tm_registers.rtc_month.month_one = (time_buffer->tm_mon % 10) + 1;
	data->tm_registers.rtc_month.month_ten = time_buffer->tm_mon / 10;
	data->tm_registers.rtc_year.year_one = year_since_epoch % 10;
	data->tm_registers.rtc_year.year_ten = year_since_epoch / 10;

	return 0;
}

/** @brief Encode time struct tm into max31343 alarm registers
 *
 * @param dev the MAX31343 device pointer.
 * @param time_buffer tm struct containing time to be encoded into max31343
 * registers.
 * @param alarm_id alarm ID, can be 0 or 1 for MAX31343.
 *
 * @retval return 0 on success, or a negative error code from invalid
 * parameter.
 */
static int encode_alarm(const struct device *dev, struct tm *time_buffer, uint8_t alarm_id)
{
	struct max31343_data *data = dev->data;

	if (alarm_id == ALARM1_ID) {
		struct max31343_alarm1_registers *alm_regs;
		uint8_t year_since_epoch;

		alm_regs = &data->alm1_registers;
		year_since_epoch = time_buffer->tm_year - UNIX_YEAR_OFFSET;

		alm_regs->alm_sec.sec_one = time_buffer->tm_sec % 10;
		alm_regs->alm_sec.sec_ten = time_buffer->tm_sec / 10;
		alm_regs->alm_min.min_one = time_buffer->tm_min % 10;
		alm_regs->alm_min.min_ten = time_buffer->tm_min / 10;
		alm_regs->alm_hours.hr_one = time_buffer->tm_hour % 10;
		alm_regs->alm_hours.hr_ten = time_buffer->tm_hour / 10;
		//alm_regs->alm_weekday.weekday = time_buffer->tm_wday + 1;
		alm_regs->alm_day_date.day_date = time_buffer->tm_mday % 10;
		alm_regs->alm_day_date.date_ten = time_buffer->tm_mday / 10;
		alm_regs->alm_month.month_one = time_buffer->tm_mon % 10 + 1;
		alm_regs->alm_month.month_ten = time_buffer->tm_mon / 10;
		alm_regs->alm_year.year_one = year_since_epoch % 10;
		alm_regs->alm_year.year_ten = year_since_epoch / 10;
	} else if (alarm_id == ALARM2_ID) {
		struct max31343_alarm2_registers *alm_regs;
		alm_regs = &data->alm2_registers;

		alm_regs->alm_min.min_one = time_buffer->tm_min % 10;
		alm_regs->alm_min.min_ten = time_buffer->tm_min / 10;
		alm_regs->alm_hours.hr_one = time_buffer->tm_hour % 10;
		alm_regs->alm_hours.hr_ten = time_buffer->tm_hour / 10;
		//alm_regs->alm_weekday.weekday = time_buffer->tm_wday;
		alm_regs->alm_day_date.day_date = time_buffer->tm_mday % 10;
		alm_regs->alm_day_date.date_ten = time_buffer->tm_mday / 10;
	} else {
		return -EINVAL;
	}

	//time_t time_unix = 0;
	//time_unix = timeutil_timegm(time_buffer);
	//LOG_INF("Alarm Time set to: %d\n", (uint32_t)time_unix);

	return 0;
}

/** @brief Reads single register from MAX31343
 *
 * @param dev the MAX31343 device pointer.
 * @param addr register address.
 * @param val pointer to uint8_t that will contain register value if
 * successful.
 *
 * @retval return 0 on success, or a negative error code from an I2C
 * transaction.
 */
static int read_register(const struct device *dev, uint8_t addr, uint8_t *val)
{
	const struct max31343_config *cfg = dev->config;

	return i2c_write_read_dt(&cfg->bus, &addr, sizeof(addr), val, 1);
}

/** @brief Read registers from device and populate max31343_registers struct
 *
 * @param dev the MAX31343 device pointer.
 * @param unix_time pointer to time_t value that will contain unix time if
 * sucessful.
 *
 * @retval return 0 on success, or a negative error code from an I2C
 * transaction.
 */
static int read_time(const struct device *dev, time_t *unix_time)
{
	struct max31343_data *data = dev->data;
	const struct max31343_config *cfg = dev->config;
	uint8_t addr = REG_RTC_SEC;

	int rc = i2c_write_read_dt(&cfg->bus, &addr, sizeof(addr),
				&data->tm_registers, RTC_TIME_REGISTERS_SIZE);

	if (rc >= 0) {
		*unix_time = decode_rtc(dev);
	}

	return rc;
}

/** @brief Write a single register to MAX31343
 *
 * @param dev the MAX31343 device pointer.
 * @param addr register address.
 * @param value Value that will be written to the register.
 *
 * @retval return 0 on success, or a negative error code from an I2C
 * transaction or invalid parameter.
 */
static int write_register(const struct device *dev, enum max31343_register addr, uint8_t value)
{
	const struct max31343_config *cfg = dev->config;
	int rc = 0;

	uint8_t time_data[2] = {addr, value};

	rc = i2c_write_dt(&cfg->bus, time_data, sizeof(time_data));

	return rc;
}

/** @brief Write a full time struct to MAX31343 registers.
 *
 * @param dev the MAX31343 device pointer.
 * @param addr first register address to write to, should be REG_RTC_SEC,
 * REG_ALM1_SEC or REG_ALM1_SEC.
 * @param size size of data struct that will be written.
 *
 * @retval return 0 on success, or a negative error code from an I2C
 * transaction or invalid parameter.
 */
static int write_data_block(const struct device *dev, enum max31343_register addr, uint8_t size)
{
	struct max31343_data *data = dev->data;
	const struct max31343_config *cfg = dev->config;
	int rc = 0;
	uint8_t time_data[MAX_WRITE_SIZE + 1];
	uint8_t *write_block_start;

	if (size > MAX_WRITE_SIZE) {
		return -EINVAL;
	}

	if (addr != REG_RTC_SEC && addr != REG_ALM1_SEC && addr != REG_ALM2_MIN) {
		return -EINVAL;
	}

	if (addr == REG_RTC_SEC) {
		write_block_start = (uint8_t *)&data->tm_registers;
	} else if (addr == REG_ALM1_SEC) {
		write_block_start = (uint8_t *)&data->alm1_registers;
	} else if (addr == REG_ALM2_MIN) {
		write_block_start = (uint8_t *)&data->alm2_registers;
	} else {
		return -EINVAL;
	}

	/* Load register address into first byte then fill in data values */
	time_data[0] = addr;
	memcpy(&time_data[1], write_block_start, size);

	rc = i2c_write_dt(&cfg->bus, time_data, size + 1);

	return rc;
}

/** @brief Sets the correct weekday.
 *
 * If the time is never set then the device defaults to 1st January 1970
 * but with the wrong weekday set. This function ensures the weekday is
 * correct in this case.
 *
 * @param dev the MAX31343 device pointer.
 * @param unix_time pointer to unix time that will be used to work out the weekday
 *
 * @retval return 0 on success, or a negative error code from an I2C
 * transaction or invalid parameter.
 */
static int set_day_of_week(const struct device *dev, time_t *unix_time)
{
	struct max31343_data *data = dev->data;
	struct tm time_buffer = { 0 };
	int rc = 0;

	gmtime_r(unix_time, &time_buffer);

	if (time_buffer.tm_wday != 0) {
		data->tm_registers.rtc_weekday.weekday = time_buffer.tm_wday - 1;
		rc = write_register(dev, REG_RTC_WDAY,
			*((uint8_t *)(&data->tm_registers.rtc_weekday)));
	} else {
		rc = -EINVAL;
	}

	return rc;
}

/** @brief Checks the interrupt pending flag (IF) of a given alarm.
 *
 * A callback is fired if an IRQ is pending.
 *
 * @param dev the MAX31343 device pointer.
 */
static void max31343_handle_interrupt(const struct device *dev)
{
	struct max31343_data *data = dev->data;
	counter_alarm_callback_t cb;
	uint32_t ticks = 0;
	bool fire_callback = false;
	uint32_t interrupt_flags;
	struct max31343_status *status;
	uint8_t alarm_id;

	k_sem_take(&data->lock, K_FOREVER);

	interrupt_flags = counter_get_pending_int(dev);
	status = (struct max31343_status *)&interrupt_flags;

	/* TODO what do ?! */
	if (status->a1f) {
		LOG_DBG("Alarm1 interrupt");
		if (data->counter_handler[ALARM1_ID]) {
			cb = data->counter_handler[ALARM1_ID];
			ticks = data->counter_ticks[ALARM1_ID];
			alarm_id = ALARM1_ID;
			fire_callback = true;
		}
	}
	if (status->a2f) {
		LOG_DBG("Alarm2 interrupt");
		if (data->counter_handler[ALARM2_ID]) {
			cb = data->counter_handler[ALARM2_ID];
			ticks = data->counter_ticks[ALARM2_ID];
			alarm_id = ALARM2_ID;
			fire_callback = true;
		}
	}
	if (status->tif) {
		LOG_DBG("Timer interrupt");
	}
	if (status->tsf) {
		LOG_DBG("Temperature measurement ready interrupt");
	}
	if (status->pfail) {
		LOG_DBG("Power fail interrupt");
	}
	if (status->osf) {
		LOG_DBG("Oscillator stop interrupt");
	}
	if (status->psdect) {
		LOG_DBG("Running on V_Bat");
	}

	k_sem_give(&data->lock);

	/* TODO what do ?! */
	if (fire_callback) {
		cb(data->max31343, 0, ticks, data->alarm_user_data[alarm_id]);
	}
}

static void max31343_work_handler(struct k_work *work)
{
	struct max31343_data *data =
		CONTAINER_OF(work, struct max31343_data, alarm_work);

	/* Check interrupt flags and act accordingly */
	max31343_handle_interrupt(data->max31343);
}

static void max31343_init_cb(const struct device *dev,
				 struct gpio_callback *gpio_cb, uint32_t pins)
{
	struct max31343_data *data =
		CONTAINER_OF(gpio_cb, struct max31343_data, int_callback);

	ARG_UNUSED(pins);

	k_work_submit(&data->alarm_work);
}

int max31343_rtc_set_time(const struct device *dev, time_t unix_time)
{
	struct max31343_data *data = dev->data;
	struct tm time_buffer = { 0 };
	int rc = 0;

	if (unix_time > UINT32_MAX) {
		LOG_ERR("Unix time must be 32-bit");
		return -EINVAL;
	}

	k_sem_take(&data->lock, K_FOREVER);

	/* Convert unix_time to civil time */
	gmtime_r(&unix_time, &time_buffer);
	LOG_DBG("Desired time is %d-%d-%d %d:%d:%d\n", (time_buffer.tm_year + 1900),
		(time_buffer.tm_mon + 1), time_buffer.tm_mday, time_buffer.tm_hour,
		time_buffer.tm_min, time_buffer.tm_sec);

	/* Encode time */
	rc = encode_rtc(dev, &time_buffer);
	if (rc < 0) {
		goto out;
	}

	/* Write to device */
	rc = write_data_block(dev, REG_RTC_SEC, RTC_TIME_REGISTERS_SIZE);

out:
	k_sem_give(&data->lock);

	return rc;
}

static int max31343_counter_start(const struct device *dev)
{
	struct max31343_data *data = dev->data;
	int rc = 0;

	k_sem_take(&data->lock, K_FOREVER);

	/* Set oscillator enable configuration bit */
	data->cfg_registers.config1.enosc = 1;
	rc = write_register(dev, REG_CONFIG1,
		*((uint8_t *)(&data->cfg_registers.config1)));

	k_sem_give(&data->lock);

	return rc;
}

static int max31343_counter_stop(const struct device *dev)
{
	struct max31343_data *data = dev->data;
	int rc = 0;

	k_sem_take(&data->lock, K_FOREVER);

	/* Clear oscillator enable configuration bit */
	data->cfg_registers.config1.enosc = 0;
	rc = write_register(dev, REG_CONFIG1,
		*((uint8_t *)(&data->cfg_registers.config1)));

	k_sem_give(&data->lock);

	return rc;
}

static int max31343_counter_get_value(const struct device *dev,
				      uint32_t *ticks)
{
	struct max31343_data *data = dev->data;
	time_t unix_time;
	int rc;

	k_sem_take(&data->lock, K_FOREVER);

	/* Get time */
	rc = read_time(dev, &unix_time);

	/* Convert time to ticks */
	if (rc >= 0) {
		*ticks = unix_time;
	}

	k_sem_give(&data->lock);

	return rc;
}

static int max31343_counter_set_alarm(const struct device *dev, uint8_t alarm_id,
				      const struct counter_alarm_cfg *alarm_cfg)
{
	struct max31343_data *data = dev->data;
	uint32_t seconds_until_alarm;
	time_t current_time;
	time_t alarm_time;
	struct tm time_buffer = { 0 };
	uint8_t alarm_base_address;
	uint8_t alarm_register_size;
	int rc = 0;


	k_sem_take(&data->lock, K_FOREVER);

	if (alarm_id == ALARM1_ID) {
		struct max31343_alarm1_registers *alm_regs;
		alarm_base_address = REG_ALM1_SEC;
		alarm_register_size = RTC_ALARM1_REGISTERS_SIZE;
		alm_regs = &data->alm1_registers;

		switch (alarm_cfg->flags) {
		case MAX31343_ALM1_SECONDLY:
			data->alm1_registers.alm_sec.a1m1 = 1;
			data->alm1_registers.alm_min.a1m2 = 1;
			data->alm1_registers.alm_hours.a1m3 = 1;
			data->alm1_registers.alm_day_date.a1m4 = 1;
			data->alm1_registers.alm_month.a1m5 = 1;
			data->alm1_registers.alm_month.a1m6 = 1;
			data->alm1_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM1_S:
			data->alm1_registers.alm_sec.a1m1 = 0;
			data->alm1_registers.alm_min.a1m2 = 1;
			data->alm1_registers.alm_hours.a1m3 = 1;
			data->alm1_registers.alm_day_date.a1m4 = 1;
			data->alm1_registers.alm_month.a1m5 = 1;
			data->alm1_registers.alm_month.a1m6 = 1;
			data->alm1_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM1_M_S:
			data->alm1_registers.alm_sec.a1m1 = 0;
			data->alm1_registers.alm_min.a1m2 = 0;
			data->alm1_registers.alm_hours.a1m3 = 1;
			data->alm1_registers.alm_day_date.a1m4 = 1;
			data->alm1_registers.alm_month.a1m5 = 1;
			data->alm1_registers.alm_month.a1m6 = 1;
			data->alm1_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM1_H_M_S:
			data->alm1_registers.alm_sec.a1m1 = 0;
			data->alm1_registers.alm_min.a1m2 = 0;
			data->alm1_registers.alm_hours.a1m3 = 0;
			data->alm1_registers.alm_day_date.a1m4 = 1;
			data->alm1_registers.alm_month.a1m5 = 1;
			data->alm1_registers.alm_month.a1m6 = 1;
			data->alm1_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM1_DT_T:
			data->alm1_registers.alm_sec.a1m1 = 0;
			data->alm1_registers.alm_min.a1m2 = 0;
			data->alm1_registers.alm_hours.a1m3 = 0;
			data->alm1_registers.alm_day_date.a1m4 = 0;
			data->alm1_registers.alm_month.a1m5 = 1;
			data->alm1_registers.alm_month.a1m6 = 1;
			data->alm1_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM1_M_DT_T:
			data->alm1_registers.alm_sec.a1m1 = 0;
			data->alm1_registers.alm_min.a1m2 = 0;
			data->alm1_registers.alm_hours.a1m3 = 0;
			data->alm1_registers.alm_day_date.a1m4 = 0;
			data->alm1_registers.alm_month.a1m5 = 0;
			data->alm1_registers.alm_month.a1m6 = 1;
			data->alm1_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM1_Y_M_DT_T:
			data->alm1_registers.alm_sec.a1m1 = 0;
			data->alm1_registers.alm_min.a1m2 = 0;
			data->alm1_registers.alm_hours.a1m3 = 0;
			data->alm1_registers.alm_day_date.a1m4 = 0;
			data->alm1_registers.alm_month.a1m5 = 0;
			data->alm1_registers.alm_month.a1m6 = 0;
			data->alm1_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM1_DY_T:
			data->alm1_registers.alm_sec.a1m1 = 0;
			data->alm1_registers.alm_min.a1m2 = 0;
			data->alm1_registers.alm_hours.a1m3 = 0;
			data->alm1_registers.alm_day_date.a1m4 = 0;
			data->alm1_registers.alm_month.a1m5 = 1;
			data->alm1_registers.alm_month.a1m6 = 1;
			data->alm1_registers.alm_day_date.dy_dt = 1;
			break;
		default:
			LOG_ERR("Invalid alarm flag received for ALM1.\n");
			rc = -EINVAL;
			goto out;
		}
	} else if (alarm_id == ALARM2_ID) {
		struct max31343_alarm2_registers *alm_regs;
		alarm_base_address = REG_ALM2_MIN;
		alarm_register_size = RTC_ALARM2_REGISTERS_SIZE;
		alm_regs = &data->alm2_registers;

		switch (alarm_cfg->flags) {
		case MAX31343_ALM2_MINUTELY:
			data->alm2_registers.alm_min.a2m2 = 1;
			data->alm2_registers.alm_hours.a2m3 = 1;
			data->alm2_registers.alm_day_date.a2m4 = 1;
			data->alm2_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM2_M:
			data->alm2_registers.alm_min.a2m2 = 0;
			data->alm2_registers.alm_hours.a2m3 = 1;
			data->alm2_registers.alm_day_date.a2m4 = 1;
			data->alm2_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM2_H_M:
			data->alm2_registers.alm_min.a2m2 = 0;
			data->alm2_registers.alm_hours.a2m3 = 0;
			data->alm2_registers.alm_day_date.a2m4 = 1;
			data->alm2_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM2_DT_H_M:
			data->alm2_registers.alm_min.a2m2 = 0;
			data->alm2_registers.alm_hours.a2m3 = 0;
			data->alm2_registers.alm_day_date.a2m4 = 0;
			data->alm2_registers.alm_day_date.dy_dt = 0;
			break;
		case MAX31343_ALM2_DY_H_M:
			data->alm2_registers.alm_min.a2m2 = 0;
			data->alm2_registers.alm_hours.a2m3 = 0;
			data->alm2_registers.alm_day_date.a2m4 = 0;
			data->alm2_registers.alm_day_date.dy_dt = 1;
			break;
		default:
			LOG_ERR("Invalid alarm flag received for ALM2.\n");
			rc = -EINVAL;
			goto out;
		}
	} else {
		LOG_ERR("Invalid alarm id received.\n");
		rc = -EINVAL;
		goto out;
	}

	/* Convert ticks to time */
	seconds_until_alarm = alarm_cfg->ticks;

	/* Get current time and add alarm offset */
	rc = read_time(dev, &current_time);
	if (rc < 0) {
		goto out;
	}

	alarm_time = current_time + seconds_until_alarm;
	gmtime_r(&alarm_time, &time_buffer);

	/* Write time to alarm registers */
	encode_alarm(dev, &time_buffer, alarm_id);
	rc = write_data_block(dev, alarm_base_address, alarm_register_size);
	if (rc < 0) {
		goto out;
	}

	/* Enable alarm */
	rc = write_register(dev, REG_INT_EN,
		*((uint8_t *)(&data->cfg_registers.int_en)));
	if (rc < 0) {
		goto out;
	}

	/* Config user data and callback */
	data->counter_handler[alarm_id] = alarm_cfg->callback;
	data->counter_ticks[alarm_id] = current_time;
	data->alarm_user_data[alarm_id] = alarm_cfg->user_data;

out:
	k_sem_give(&data->lock);

	return rc;
}

static int max31343_counter_cancel_alarm(const struct device *dev, uint8_t alarm_id)
{
	struct max31343_data *data = dev->data;
	int rc = 0;

	k_sem_take(&data->lock, K_FOREVER);

	/* Clear alarm enable bit */
	if (alarm_id == ALARM1_ID) {
		data->cfg_registers.int_en.a1e = 0;
	} else if (alarm_id == ALARM2_ID) {
		data->cfg_registers.int_en.a2e = 0;
	} else {
		rc = -EINVAL;
		goto out;
	}

	rc = write_register(dev, REG_INT_EN,
		*((uint8_t *)(&data->cfg_registers.int_en)));

out:
	k_sem_give(&data->lock);

	return rc;
}

static int max31343_counter_set_top_value(const struct device *dev,
					  const struct counter_top_cfg *cfg)
{
	return -ENOTSUP;
}

/*
 * This function can be used to poll the interrupt flags if no int-gpios is
 * configured on the MAX31343 *INT pin.
 * It can also be used to check if an interrupt was triggered while the MCU was in reset.
 * Reading the status register always clears all interrupt flags.
 *
 * @param dev the MAX31343 device pointer.
 *
 * @return bitmask of alarm interrupt flag (IF) where each IF is shifted by
 * the alarm ID.
 */
static uint32_t max31343_counter_get_pending_int(const struct device *dev)
{
	struct max31343_data *data = dev->data;
	uint8_t interrupt_flags = 0;
	int rc;

	k_sem_take(&data->lock, K_FOREVER);

	rc = read_register(dev, REG_STATUS, (uint8_t *)&data->status_flags);
	if (rc < 0) {
		goto out;
	}

	interrupt_flags = *((uint8_t *)&data->status_flags);
out:
	k_sem_give(&data->lock);

	return interrupt_flags;
}

static uint32_t max31343_counter_get_top_value(const struct device *dev)
{
	return UINT32_MAX;
}

static int max31343_reset(const struct device *dev)
{
	struct max31343_reset res = { .swrst = 1 };
	int rc;

	rc = write_register(dev, REG_RESET, *((uint8_t *)&res));
	if (rc) {
		return rc;
	}

	res.swrst = 0;
	rc = write_register(dev, REG_RESET, *((uint8_t *)&res));
	if (rc) {
		return rc;
	}

	return rc;
}

static int max31343_init(const struct device *dev)
{
	struct max31343_data *data = dev->data;
	const struct max31343_config *cfg = dev->config;
	int rc = 0;
	time_t unix_time = 0;

	/* Initialize and take the lock */
	k_sem_init(&data->lock, 0, 1);

	if (!device_is_ready(cfg->bus.bus)) {
		LOG_ERR("I2C device is not ready");
		rc = -ENODEV;
		goto out;
	}

	rc = read_time(dev, &unix_time);
	if (rc < 0) {
		goto out;
	}

	rc = set_day_of_week(dev, &unix_time);
	if (rc < 0) {
		goto out;
	}

	//struct max31343_trickle trick;
	//trick.d_trickle = 5;
	//trick.tche = 5;
	//rc = write_register(dev, REG_TRICKLE, *((uint8_t *)&trick));
	//if (rc < 0) {
	//	goto out;
	//}

	rc = max31343_reset(dev);
	if (rc < 0) {
		goto out;
	}

	/* Configure alarm interrupt gpio */
	if (cfg->int_gpios.port != NULL) {

		if (!device_is_ready(cfg->int_gpios.port)) {
			LOG_ERR("Port device %s is not ready",
				cfg->int_gpios.port->name);
			rc = -ENODEV;
			goto out;
		}

		data->max31343 = dev;
		k_work_init(&data->alarm_work, max31343_work_handler);

		gpio_pin_configure_dt(&cfg->int_gpios, GPIO_INPUT);

		gpio_pin_interrupt_configure_dt(&cfg->int_gpios,
						GPIO_INT_EDGE_TO_ACTIVE);

		gpio_init_callback(&data->int_callback, max31343_init_cb,
				   BIT(cfg->int_gpios.pin));

		gpio_add_callback(cfg->int_gpios.port, &data->int_callback);
	}
out:
	k_sem_give(&data->lock);

	return rc;
}

static const struct counter_driver_api max31343_api = {
	.start = max31343_counter_start,
	.stop = max31343_counter_stop,
	.get_value = max31343_counter_get_value,
	.set_alarm = max31343_counter_set_alarm,
	.cancel_alarm = max31343_counter_cancel_alarm,
	.set_top_value = max31343_counter_set_top_value,
	.get_pending_int = max31343_counter_get_pending_int,
	.get_top_value = max31343_counter_get_top_value,
};

#define MAX31343_INIT(n)								\
	static struct max31343_data max31343_data_##n;					\
	static const struct max31343_config max31343_config_##n = {			\
		.generic = {								\
			.max_top_value = UINT32_MAX,					\
			.freq = 1,							\
			.flags = COUNTER_CONFIG_INFO_COUNT_UP,				\
			.channels = 2,							\
		},									\
		.bus = I2C_DT_SPEC_INST_GET(n),						\
		.int_gpios =  GPIO_DT_SPEC_GET_OR(DT_DRV_INST(n), int_gpios, {0}),	\
	};										\
	DEVICE_DT_INST_DEFINE(n, max31343_init, NULL,					\
		    &max31343_data_##n,							\
		    &max31343_config_##n,						\
		    POST_KERNEL,							\
		    CONFIG_MAX31343_INIT_PRIORITY,					\
		    &max31343_api);

DT_INST_FOREACH_STATUS_OKAY(MAX31343_INIT);
