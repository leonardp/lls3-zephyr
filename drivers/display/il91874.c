/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT ilitek_il91874

#include <string.h>
#include <device.h>
#include <init.h>
#include <drivers/display.h>
#include <drivers/gpio.h>
#include <drivers/spi.h>
#include <sys/byteorder.h>

#include "il91874_regs.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(il91874, CONFIG_DISPLAY_LOG_LEVEL);

/**
 * IL91874 compatible EPD controller driver.
 *
 * Currently only the black/white pannels are supported (KW mode),
 * also first gate/source should be 0.
 */

#define IL91874_DC_PIN DT_INST_GPIO_PIN(0, dc_gpios)
#define IL91874_DC_FLAGS DT_INST_GPIO_FLAGS(0, dc_gpios)
#define IL91874_DC_CNTRL DT_INST_GPIO_LABEL(0, dc_gpios)
#define IL91874_BUSY_PIN DT_INST_GPIO_PIN(0, busy_gpios)
#define IL91874_BUSY_CNTRL DT_INST_GPIO_LABEL(0, busy_gpios)
#define IL91874_BUSY_FLAGS DT_INST_GPIO_FLAGS(0, busy_gpios)
#define IL91874_RESET_PIN DT_INST_GPIO_PIN(0, reset_gpios)
#define IL91874_RESET_CNTRL DT_INST_GPIO_LABEL(0, reset_gpios)
#define IL91874_RESET_FLAGS DT_INST_GPIO_FLAGS(0, reset_gpios)

#define EPD_PANEL_WIDTH			DT_INST_PROP(0, width)
#define EPD_PANEL_HEIGHT		DT_INST_PROP(0, height)
#define IL91874_PIXELS_PER_BYTE		8U

/* Horizontally aligned page! */
#define IL91874_NUMOF_PAGES		(EPD_PANEL_WIDTH / \
					 IL91874_PIXELS_PER_BYTE)

struct il91874_data {
	const struct il91874_config *config;
	uint8_t ptl_window[IL91874_PTL_PARAM_LENGTH];
	const struct device *reset;
	const struct device *dc;
	const struct device *busy;
};

struct il91874_config {
	struct spi_dt_spec bus;
};

static uint8_t il91874_vdcs = DT_INST_PROP(0, vdcs);
static uint8_t il91874_cdi = DT_INST_PROP(0, cdi);
static uint8_t il91874_cdi_partial = DT_INST_PROP(0, cdi_partial);
static uint8_t il91874_lut_vcom[] = DT_INST_PROP(0, lut_vcom);
static uint8_t il91874_lut_ww[] = DT_INST_PROP(0, lut_ww);
static uint8_t il91874_lut_bw[] = DT_INST_PROP(0, lut_bw);
static uint8_t il91874_lut_wb[] = DT_INST_PROP(0, lut_wb);
static uint8_t il91874_lut_bb[] = DT_INST_PROP(0, lut_bb);
static uint8_t il91874_lut_vcom_partial[] = DT_INST_PROP(0, lut_vcom_partial);
static uint8_t il91874_lut_ww_partial[] = DT_INST_PROP(0, lut_ww_partial);
static uint8_t il91874_lut_bw_partial[] = DT_INST_PROP(0, lut_bw_partial);
static uint8_t il91874_lut_wb_partial[] = DT_INST_PROP(0, lut_wb_partial);
static uint8_t il91874_lut_bb_partial[] = DT_INST_PROP(0, lut_bb_partial);

static bool blanking_on = true;

static inline int il91874_write_cmd(struct il91874_data *driver,
				   uint8_t cmd, uint8_t *data, size_t len)
{
	struct spi_buf buf = {.buf = &cmd, .len = sizeof(cmd)};
	struct spi_buf_set buf_set = {.buffers = &buf, .count = 1};

	gpio_pin_set(driver->dc, IL91874_DC_PIN, 1);
	if (spi_write_dt(&driver->config->bus, &buf_set)) {
		return -EIO;
	}

	if (data != NULL) {
		int i;

		gpio_pin_set(driver->dc, IL91874_DC_PIN, 0);
		/*
		buf.buf = data;
		buf.len = len;
		if (spi_write_dt(&driver->config->bus, &buf_set)) {
			return -EIO;
		}
		*/
		/*  */
		if (cmd == IL91874_CMD_PDTM1 ||	cmd == IL91874_CMD_PDTM2) {
			for (i = 0; i < IL91874_PTL_PARAM_LENGTH; i++)
			{
				buf.buf = driver->ptl_window+i;
				buf.len = 1;
				if (spi_write_dt(&driver->config->bus, &buf_set)) {
					return -EIO;
				}
			}
		}

		/* Needs CS to increment register pointer?! */
		for (i = 0; i < len; i++)
		{
			buf.buf = data+i;
			buf.len = 1;
			if (spi_write_dt(&driver->config->bus, &buf_set)) {
				return -EIO;
			}
		}
		gpio_pin_set(driver->dc, IL91874_DC_PIN, 1);
	}

	return 0;
}

static inline void il91874_busy_wait(struct il91874_data *driver)
{
	int pin = gpio_pin_get(driver->busy, IL91874_BUSY_PIN);

	while (pin > 0) {
		__ASSERT(pin >= 0, "Failed to get pin level");
		//LOG_DBG("wait %u", pin);
		k_sleep(K_MSEC(IL91874_BUSY_DELAY));
		pin = gpio_pin_get(driver->busy, IL91874_BUSY_PIN);
	}
}

static int il91874_set_full_lut(const struct device *dev)
{
	struct il91874_data *driver = dev->data;

	if (il91874_write_cmd(driver, IL91874_CMD_CDI, &il91874_cdi, 1)) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_VCOM,
				il91874_lut_vcom, sizeof(il91874_lut_vcom))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_WW,
				il91874_lut_ww, sizeof(il91874_lut_ww))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_BW,
				il91874_lut_bw, sizeof(il91874_lut_bw))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_WB,
				il91874_lut_wb, sizeof(il91874_lut_wb))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_BB,
				il91874_lut_bb, sizeof(il91874_lut_bb))) {
		return -EIO;
	}

	return 0;
}

static int il91874_set_partial_lut(const struct device *dev)
{
	struct il91874_data *driver = dev->data;

	if (il91874_write_cmd(driver, IL91874_CMD_CDI, &il91874_cdi_partial, 1)) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_VCOM,
			il91874_lut_vcom_partial, sizeof(il91874_lut_vcom_partial))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_WW,
			il91874_lut_ww_partial, sizeof(il91874_lut_ww_partial))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_BW,
			il91874_lut_bw_partial, sizeof(il91874_lut_bw_partial))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_WB,
			il91874_lut_wb_partial, sizeof(il91874_lut_wb_partial))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_LUT_BB,
			il91874_lut_bb_partial, sizeof(il91874_lut_bb_partial))) {
		return -EIO;
	}

	return 0;
}

static int il91874_update_display(const struct device *dev)
{
	struct il91874_data *driver = dev->data;

	LOG_DBG("Trigger full display refresh");

	/* Write LUT for full refresh */
	if (il91874_set_full_lut(dev)) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_DRF, NULL, 0)) {
		return -EIO;
	}

	k_sleep(K_MSEC(IL91874_BUSY_DELAY));

	il91874_busy_wait(driver);

	/* Write LUT for partial refresh */
	if (il91874_set_partial_lut(dev)) {
		return -EIO;
	}

	return 0;
}

static int il91874_blanking_off(const struct device *dev)
{
	struct il91874_data *driver = dev->data;

	if (blanking_on) {
		/* Update EPD pannel in normal mode */
		il91874_busy_wait(driver);
		if (il91874_update_display(dev)) {
			return -EIO;
		}
	}

	blanking_on = false;

	return 0;
}

static int il91874_blanking_on(const struct device *dev)
{
	blanking_on = true;

	return 0;
}

static int il91874_write(const struct device *dev, const uint16_t x, const uint16_t y,
			const struct display_buffer_descriptor *desc,
			const void *buf)
{
	struct il91874_data *driver = dev->data;
	uint16_t x_end_idx = x + desc->width - 1;
	uint16_t y_end_idx = y + desc->height - 1;
	size_t buf_len;

	LOG_DBG("x %u, y %u, height %u, width %u, pitch %u",
		x, y, desc->height, desc->width, desc->pitch);

	buf_len = MIN(desc->buf_size,
		      desc->height * desc->width / IL91874_PIXELS_PER_BYTE);

	__ASSERT(desc->width <= desc->pitch, "Pitch is smaller then width");
	__ASSERT(buf != NULL, "Buffer is not available");
	__ASSERT(buf_len != 0U, "Buffer of length zero");
	__ASSERT(!(desc->width % IL91874_PIXELS_PER_BYTE),
		 "Buffer width not multiple of %d", IL91874_PIXELS_PER_BYTE);

	if ((y_end_idx > (EPD_PANEL_HEIGHT - 1)) ||
	    (x_end_idx > (EPD_PANEL_WIDTH - 1))) {
		LOG_ERR("Position out of bounds");
		return -EINVAL;
	}

	/* Setup Partial Window */
	sys_put_be16(x, &driver->ptl_window[IL91874_PTL_X_IDX]);
	sys_put_be16(y, &driver->ptl_window[IL91874_PTL_Y_IDX]);
	sys_put_be16(desc->width, &driver->ptl_window[IL91874_PTL_WIDTH_IDX]);
	sys_put_be16(desc->height, &driver->ptl_window[IL91874_PTL_HEIGHT_IDX]);

	il91874_busy_wait(driver);

	if (il91874_write_cmd(driver, IL91874_CMD_PDTM2, (uint8_t *)buf, buf_len)) {
		return -EIO;
	}

	if (blanking_on == false) {
		//driver->ptl_window[0] |= IL91874_DFV_EN;
		if (il91874_write_cmd(driver, IL91874_CMD_PDRF, (uint8_t *)&driver->ptl_window, IL91874_PTL_PARAM_LENGTH)) {
			return -EIO;
		}
	}

	il91874_busy_wait(driver);

	/* write "old" data */
	if (il91874_write_cmd(driver, IL91874_CMD_PDTM1, (uint8_t *)buf, buf_len)) {
		return -EIO;
	}

	return 0;
}

static int il91874_read(const struct device *dev, const uint16_t x, const uint16_t y,
		       const struct display_buffer_descriptor *desc, void *buf)
{
	LOG_ERR("not supported by display!");
	return -ENOTSUP;
}

static void *il91874_get_framebuffer(const struct device *dev)
{
	LOG_ERR("not supported by display!");
	return NULL;
}

static int il91874_set_brightness(const struct device *dev,
				 const uint8_t brightness)
{
	LOG_ERR("not supported by display!");
	return -ENOTSUP;
}

static int il91874_set_contrast(const struct device *dev, uint8_t contrast)
{
	LOG_ERR("not supported by display!");
	return -ENOTSUP;
}

static void il91874_get_capabilities(const struct device *dev,
				    struct display_capabilities *caps)
{
	memset(caps, 0, sizeof(struct display_capabilities));
	caps->x_resolution = EPD_PANEL_WIDTH;
	caps->y_resolution = EPD_PANEL_HEIGHT;
	caps->supported_pixel_formats = PIXEL_FORMAT_MONO10;
	caps->current_pixel_format = PIXEL_FORMAT_MONO10;
	caps->screen_info = SCREEN_INFO_MONO_MSB_FIRST | SCREEN_INFO_EPD;
}

static int il91874_set_orientation(const struct device *dev,
				  const enum display_orientation
				  orientation)
{
	LOG_ERR("not supported by display!");
	return -ENOTSUP;
}

static int il91874_set_pixel_format(const struct device *dev,
				   const enum display_pixel_format pf)
{
	if (pf == PIXEL_FORMAT_MONO10) {
		return 0;
	}

	LOG_ERR("not supported by display!");
	return -ENOTSUP;
}

static int il91874_clear_and_write_buffer(const struct device *dev,
					 uint8_t pattern, bool update)
{
	struct display_buffer_descriptor desc = {
		.buf_size = IL91874_NUMOF_PAGES,
		.width = EPD_PANEL_WIDTH,
		.height = 1,
		.pitch = EPD_PANEL_WIDTH,
	};
	uint8_t *line;

	line = k_malloc(IL91874_NUMOF_PAGES);
	if (line == NULL) {
		return -ENOMEM;
	}

	memset(line, pattern, IL91874_NUMOF_PAGES);
	for (int i = 0; i < EPD_PANEL_HEIGHT; i++) {
		il91874_write(dev, 0, i, &desc, line);
	}

	k_free(line);

	if (update == true) {
		if (il91874_update_display(dev)) {
			return -EIO;
		}
	}

	return 0;
}

static int il91874_controller_init(const struct device *dev)
{
	struct il91874_data *driver = dev->data;
	static uint8_t il91874_pwr[] = DT_INST_PROP(0, pwr);
	static uint8_t il91874_softstart[] = DT_INST_PROP(0, softstart);
	uint8_t tmp[IL91874_TRES_REG_LENGTH];

	gpio_pin_set(driver->reset, IL91874_RESET_PIN, 1);
	k_sleep(K_MSEC(IL91874_RESET_DELAY));
	gpio_pin_set(driver->reset, IL91874_RESET_PIN, 0);
	k_sleep(K_MSEC(IL91874_RESET_DELAY));
	il91874_busy_wait(driver);

	LOG_DBG("Initialize IL91874 controller");

	if (il91874_write_cmd(driver, IL91874_CMD_PWR,
				il91874_pwr, sizeof(il91874_pwr))) {
		return -EIO;
	}

	if (il91874_write_cmd(driver, IL91874_CMD_BTST,
			     il91874_softstart, sizeof(il91874_softstart))) {
		return -EIO;
	}

	/* Reset DFV_EN */
	tmp[0] = 0x00;
	if (il91874_write_cmd(driver, IL91874_CMD_PDRF, tmp, 1)) {
		return -EIO;
	}

	/* MAGIC Panel settings, KW mode */
	tmp[0] = 0xbf;
	if (il91874_write_cmd(driver, IL91874_CMD_PSR, tmp, 1)) {
		return -EIO;
	}

	/* MAGIC PLL Settings */
	tmp[0] = 0x3a;
	if (il91874_write_cmd(driver, IL91874_CMD_OSC, tmp, 1)) {
		return -EIO;
	}

	/* Set panel resolution */
	sys_put_be16((EPD_PANEL_WIDTH), &tmp[IL91874_TRES_HRES_IDX]);
	sys_put_be16((EPD_PANEL_HEIGHT), &tmp[IL91874_TRES_VRES_IDX]);
	if (il91874_write_cmd(driver, IL91874_CMD_TRES,
			     tmp, IL91874_TRES_REG_LENGTH)) {
		return -EIO;
	}

	/* VDCS Setting */
	if (il91874_write_cmd(driver, IL91874_CMD_VDCS,	&il91874_vdcs, 1)) {
		return -EIO;
	}

	/* Vcom and data interval setting */
	if (il91874_write_cmd(driver, IL91874_CMD_CDI, &il91874_cdi_partial, 1)) {
		return -EIO;
	}

	/* Write LUT for partial refresh */
	if (il91874_set_partial_lut(dev)) {
		return -EIO;
	}

	/* Power On */
	if (il91874_write_cmd(driver, IL91874_CMD_PON, NULL, 0)) {
		return -EIO;
	}

	k_sleep(K_MSEC(IL91874_PON_DELAY));
	il91874_busy_wait(driver);

	if (il91874_clear_and_write_buffer(dev, 0xFF, false)) {
		return -1;
	}

	return 0;
}

static int il91874_init(const struct device *dev)
{
	const struct il91874_config *config = dev->config;
	struct il91874_data *driver = dev->data;

	if (!spi_is_ready(&config->bus)) {
		LOG_ERR("SPI bus %s not ready", config->bus.bus->name);
		return -ENODEV;
	}

	driver->reset = device_get_binding(IL91874_RESET_CNTRL);
	if (driver->reset == NULL) {
		LOG_ERR("Could not get GPIO port for IL91874 reset");
		return -EIO;
	}

	gpio_pin_configure(driver->reset, IL91874_RESET_PIN,
			   GPIO_OUTPUT_INACTIVE | IL91874_RESET_FLAGS);

	driver->dc = device_get_binding(IL91874_DC_CNTRL);
	if (driver->dc == NULL) {
		LOG_ERR("Could not get GPIO port for IL91874 DC signal");
		return -EIO;
	}

	gpio_pin_configure(driver->dc, IL91874_DC_PIN,
			   GPIO_OUTPUT_INACTIVE | IL91874_DC_FLAGS);

	driver->busy = device_get_binding(IL91874_BUSY_CNTRL);
	if (driver->busy == NULL) {
		LOG_ERR("Could not get GPIO port for IL91874 busy signal");
		return -EIO;
	}

	gpio_pin_configure(driver->busy, IL91874_BUSY_PIN,
			   GPIO_INPUT | IL91874_BUSY_FLAGS);

	return il91874_controller_init(dev);
}

static const struct il91874_config il91874_config = {
	.bus = SPI_DT_SPEC_INST_GET(
		0, SPI_OP_MODE_MASTER | SPI_WORD_SET(8), 0)
};

static struct il91874_data il91874_driver = {
	.config = &il91874_config
};

static struct display_driver_api il91874_driver_api = {
	.blanking_on = il91874_blanking_on,
	.blanking_off = il91874_blanking_off,
	.write = il91874_write,
	.read = il91874_read,
	.get_framebuffer = il91874_get_framebuffer,
	.set_brightness = il91874_set_brightness,
	.set_contrast = il91874_set_contrast,
	.get_capabilities = il91874_get_capabilities,
	.set_pixel_format = il91874_set_pixel_format,
	.set_orientation = il91874_set_orientation,
};


DEVICE_DT_INST_DEFINE(0, il91874_init, NULL,
		      &il91874_driver, &il91874_config,
		      POST_KERNEL, CONFIG_DISPLAY_INIT_PRIORITY,
		      &il91874_driver_api);
