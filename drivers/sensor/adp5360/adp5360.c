/*
 * Copyright (c) 2021 Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT adi_adp5360

#include <device.h>
#include <drivers/i2c.h>
#include <kernel.h>
#include <drivers/sensor.h>
#include <logging/log.h>
#include <sys/byteorder.h>
#include <sys/crc.h>

#include <drivers/sensor/adp5360.h>
#include "adp5360.h"

LOG_MODULE_REGISTER(ADP5360, CONFIG_SENSOR_LOG_LEVEL);

static int adp5360_reg_read(const struct device *dev,
		uint8_t addr,
		uint8_t *val)
{
	const struct adp5360_config *cfg = dev->config;
	int rc;

	rc = i2c_write_read_dt(&cfg->bus,
			&addr, sizeof(addr),
			val, sizeof(val));

	return rc;
}

static int adp5360_reg_write(const struct device *dev,
		uint8_t addr,
		uint8_t *val)
{
	const struct adp5360_config *cfg = dev->config;
	uint8_t tx_buf[2];

	tx_buf[0] = addr;
	tx_buf[1] = *val;

	return i2c_write_dt(&cfg->bus, tx_buf, sizeof(tx_buf));
}

static int adp5360_attr_set(const struct device *dev,
				enum sensor_channel chan,
				enum sensor_attribute attr,
				const struct sensor_value *val)
{
	//struct adp5360_data *data = dev->data;

	switch ((enum sensor_attribute_adp5360)attr) {
	case SENSOR_ATTR_ADP5360_ASDF:
	{
	}
		break;
	case SENSOR_ATTR_ADP5360_GHJK:
	{
	}
		break;
	default:
		return -ENOTSUP;
	}
	return 0;
}

static int adp5360_sample_fetch(const struct device *dev,
			       enum sensor_channel chan)
{
	//struct adp5360_data *data = dev->data;
	//const struct adp5360_config *cfg = dev->config;
	//int rc;


	return 0;
}

static int adp5360_channel_get(const struct device *dev,
			      enum sensor_channel chan,
			      struct sensor_value *val)
{
	//const struct adp5360_data *data = dev->data;

	return 0;
}


#ifdef CONFIG_PM_DEVICE
static int adp5360_pm_action(const struct device *dev, enum pm_device_action action)
{
	uint8_t val;

	switch (action) {
	case PM_DEVICE_ACTION_RESUME:
		val = 0x00;
		//break;
	case PM_DEVICE_ACTION_SUSPEND:
		val = 0x00;
		//break;
	default:
		return -ENOTSUP;
	}

	//return adp5360_reg_write(dev, REG_ADDR, val);
	return 0;
}
#endif /* CONFIG_PM_DEVICE */

static int adp5360_init(const struct device *dev)
{
	const struct adp5360_config *cfg = dev->config;

	if (!device_is_ready(cfg->bus.bus)) {
		LOG_ERR("Device not ready.");
		return -ENODEV;
	}

	return 0;
}

static const struct sensor_driver_api adp5360_api = {
	.sample_fetch = adp5360_sample_fetch,
	.channel_get = adp5360_channel_get,
	.attr_set = adp5360_attr_set,
};

#define ADP5360_INIT(inst)						\
	static struct adp5360_data adp5360_data_##inst;			\
									\
	static const struct adp5360_config adp5360_config_##inst = {	\
		.bus = I2C_DT_SPEC_INST_GET(inst),			\
	};								\
									\
	PM_DEVICE_DT_INST_DEFINE(inst, adp5360_pm_action);		\
									\
	DEVICE_DT_INST_DEFINE(inst,					\
				adp5360_init,				\
				PM_DEVICE_DT_INST_REF(inst),		\
				&adp5360_data_##inst,			\
				&adp5360_config_##inst,			\
				POST_KERNEL,				\
				CONFIG_SENSOR_INIT_PRIORITY,		\
				&adp5360_api);


DT_INST_FOREACH_STATUS_OKAY(ADP5360_INIT)
