# Copyright 2021 Leonard Pollak
# SPDX-License-Identifier: Apache-2.0

description: Battery

include: [base.yaml]

compatible: "battery"

description: |
  Batteries must be referenced by chargers and/or fuel-gauges using a phandle.
  The phandle's property should be named "monitored-battery".

properties:
  label:
    required: true

  over-voltage-threshold-microvolt:
    type: int
    required: false
    description: battery over-voltage limit

  re-charge-voltage-microvolt:
    type: int
    required: false
    description: limit to automatically start charging again

  voltage-min-design-microvolt:
    type: int
    required: false
    description: drained battery voltage

  voltage-max-design-microvolt:
    type: int
    required: false
    description: fully charged battery voltage

  energy-full-design-microwatt-hours:
    type: int
    required: false
    description: battery design energy

  charge-full-design-microamp-hours:
    type: int
    required: false
    description: battery design capacity

  trickle-charge-current-microamp:
    type: int
    required: false
    description: current for trickle-charge phase

  precharge-current-microamp:
    type: int
    required: false
    description: current for pre-charge phase

  precharge-upper-limit-microvolt:
    type: int
    required: false
    description: limit when to change to constant charging

  charge-term-current-microamp:
    type: int
    required: false
    description: current for charge termination phase

  constant-charge-current-max-microamp:
    type: int
    required: false
    description: maximum constant input current

  constant-charge-voltage-max-microvolt:
    type: int
    required: false
    description: maximum constant input voltage
