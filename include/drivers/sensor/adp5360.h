/*
 * Copyright (c) 2021, Leonard Pollak
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * @file
 * @brief Extended public API for the Analog Device's ADP5360
 *
 * This exposes attributes for the ADP5360 which can be used for
 * ---
 */

#ifndef ZEPHYR_INCLUDE_DRIVERS_SENSOR_ADP5360_H_
#define ZEPHYR_INCLUDE_DRIVERS_SENSOR_ADP5360_H_

#include <drivers/sensor.h>

#ifdef __cplusplus
extern "C" {
#endif

enum sensor_attribute_adp5360 {
	SENSOR_ATTR_ADP5360_ASDF = SENSOR_ATTR_PRIV_START,
	SENSOR_ATTR_ADP5360_GHJK
};

#ifdef __cplusplus
}
#endif

#endif /* ZEPHYR_INCLUDE_DRIVERS_SENSOR_ADP5360_H_ */
